FIIT STU Thesis template for LaTeX
==================================

* Book, two-sided
* ISO 690 bibliography

Example: [PDF](https://bitbucket.org/m_by/fiit-stu-thesis-template-for-latex/src/a5257f39ea8923b6800ae06609668d9e41a97a66/template/main.pdf?at=master)

Install LaTeX
-------------

    apt-get install texlive
    apt-get install texlive-latex-extra
    apt-get install biblatex biblatex-dw

Install ISO 690
---------------

* Download biblatex-iso690:
https://github.com/michal-h21/biblatex-iso690

* Find the texmfdir directory:
kpsewhich -var-value TEXMFHOME

Usually, it is located here:

~/texmf/tex/latex/biblatex-chem on Linux (~ = your home folder)

~/Library/texmf/tex/latex/biblatex-chem on Mac OS X (~ = your home folder)

<USERPROFILE>\texmf\tex\latex\biblatex-chem on Windows (<USERPROFILE> = your home folder)

* Extract bib-latex-iso690 to $TEXMFHOME/tex/latex/biblatex-iso690-master/

Compile and enjoy
-----------------

    pdflatex main
    bibtex main
    pdflatex main


